"use strict";
var util = require("./util");
var extend = require("util")._extend;
var BPromise = require("bluebird");
var RedisAdapter = require("./sessionAdapters/RedisAdapter");
var MemoryAdapter = require("./sessionAdapters/MemoryAdapter");
var FileAdapter = require("./sessionAdapters/FileAdapter");

var tokenPrefix = "token";

function Session(config) {
    var adapter;
    var sessionAdapter = config.getItem("session.adapter");
    if (sessionAdapter === "redis") {
        adapter = new RedisAdapter(config);
    } else if (sessionAdapter === "file") {
        adapter = new FileAdapter(config);
    } else {
        adapter = new MemoryAdapter();
    }
    this._adapter = adapter;
}

module.exports = Session;

Session.prototype.storeToken = function (token) {
    console.log("storeToken", token.key, token.expires - Date.now());
    var self = this;
    token = extend({}, token);
    if (!token.password && token.salt && token.derived_key) {
        return this._adapter
            .storeKey(
                tokenPrefix + ":" + token.key,
                token.expires - Date.now(),
                JSON.stringify(token)
            )
            .then(function () {
                delete token.salt;
                delete token.derived_key;
                return BPromise.resolve(token);
            });
    }
    return util
        .hashPassword(token.password)
        .then(function (hash) {
            token.salt = hash.salt;
            token.derived_key = hash.derived_key;
            delete token.password;
            return self._adapter.storeKey(
                tokenPrefix + ":" + token.key,
                token.expires - Date.now(),
                JSON.stringify(token)
            );
        })
        .then(function () {
            delete token.salt;
            delete token.derived_key;
            return BPromise.resolve(token);
        });
};

Session.prototype.deleteTokens = function (keys) {
    var entries = [];
    if (!(keys instanceof Array)) {
        keys = [keys];
    }
    keys.forEach(function (key) {
        entries.push(tokenPrefix + ":" + key);
    });
    return this._adapter.deleteKeys(entries);
};

function mapUserToToken(user) {
    return {
        provider: "local",
        roles: ["user"],
        expires: user.expires,
        issued: user.issued,
        key: user.name,
        _id: user.user_id,
        salt: user.salt,
        derived_key: user.derived_key,
    };
}

Session.prototype.confirmToken = function (key, password, couchAuthDB) {
    var self = this;
    var token;
    return (
        this._adapter
            .getKey(tokenPrefix + ":" + key)
            // If no result from adapter (redis)
            .then(function (result) {
                if (!result) {
                    // Search the couch auth db for a document with this key
                    return couchAuthDB
                        .get("org.couchdb.user:" + key)
                        .then((userDoc) => {
                            // Check if the expires time is in the past
                            if (userDoc.expires < Date.now()) {
                                return BPromise.reject("expired token");
                            }
                            const token = mapUserToToken(userDoc);
                            self.storeToken({ ...token });
                            return JSON.stringify(token);
                        });
                }
                return BPromise.resolve(result);
            })
            .then(function (result) {
                if (!result) {
                    console.log("no result");
                    return BPromise.reject("invalid token");
                }
                token = JSON.parse(result);
                return util.verifyPassword(token, password);
            })
            .then(
                function () {
                    delete token.salt;
                    delete token.derived_key;
                    return BPromise.resolve(token);
                },
                function () {
                    return BPromise.reject("invalid token");
                }
            )
    );
};

Session.prototype.fetchToken = function (key) {
    console.log("fetchToken", key);
    return this._adapter
        .getKey(tokenPrefix + ":" + key)
        .catch(function (e) {
            console.log(e);
        })
        .then(function (result) {
            return BPromise.resolve(JSON.parse(result));
        });
};

Session.prototype.quit = function () {
    return this._adapter.quit();
};
